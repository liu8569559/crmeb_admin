// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2021 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------

/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const marketingRouter = {
  path: '/marketing',
  component: Layout,
  redirect: '/coupon/list',
  name: 'Marketing',
  meta: {
    title: '营销',
    icon: 'clipboard'
  },
  children: [
    {
      path: 'coupon',
      component: () => import('@/views/marketing/coupon/index'),
      name: 'Coupon',
      meta: { title: '优惠券', icon: '' },
      children: [
        {
          path: 'template',
          component: () => import('@/views/marketing/coupon/couponTemplate/index'),
          name: 'couponTemplate',
          hidden: true,
          meta: { title: '优惠券模板', icon: '' }
        },
        {
          path: 'list/save/:id?',
          name: 'couponAdd',
          meta: {
            title: '优惠劵添加',
            noCache: true,
            activeMenu: `/marketing/coupon/list`
          },
          hidden: true,
          component: () => import('@/views/marketing/coupon/list/creatCoupon')
        },
        {
          path: 'list',
          component: () => import('@/views/marketing/coupon/list/index'),
          name: 'List',
          meta: { title: '优惠券列表', icon: '' }
        },
        {
          path: 'record',
          component: () => import('@/views/marketing/coupon/record/index'),
          name: 'Record',
          meta: { title: '领取记录', icon: '' }
        }
      ]
    },
  ]
}

export default marketingRouter
